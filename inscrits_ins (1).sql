-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 16 Décembre 2015 à 04:12
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bd_kaggle`
--

-- --------------------------------------------------------

--
-- Structure de la table `inscrits_ins`
--

CREATE TABLE IF NOT EXISTS `inscrits_ins` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Prénom` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Mail` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Password` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `statut` varchar(200) NOT NULL,
  `Id_equ` int(200) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `Id_equ` (`Id_equ`),
  KEY `Id_equ_2` (`Id_equ`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `inscrits_ins`
--

INSERT INTO `inscrits_ins` (`Id`, `Nom`, `Prénom`, `Mail`, `Password`, `statut`, `Id_equ`) VALUES
(1, 'a', 'b', 'mina@hotmail.fr', '$1$Tr0.gc0.$fyPn2AmLzNLFqwmeoZ5bo.', '', 0),
(2, 'uli', 'uli', 'u@li.fr', '$1$nf0.kC..$0/0HOar4NQFxJpWhtClZ41', '', 0),
(3, 'absa', 'gueye', 'a.g@htm.com', '$1$cD../c..$kcL5oeHpSN0KYvUaIA/Ma/', '', 0),
(4, 'victor', 'vente', 'v@htm.fr', '$1$515.oP3.$eBAq40FubH/KxNa8qcE1p0', '', 0),
(5, 'fatou', 'mbaye', 'fa@hotmail.fr', '$1$M.2.ld2.$LoDeDmhnHazrtDFe6RL2U.', 'equipier', 0),
(6, 'fatou', 'mbaye', 'fa@hotmail.fr', '$1$nz5.kG1.$nsmWcP3c1M.0RpO347EJ91', 'equipier', 0),
(7, 'diop', 'oumy', 'old@hotmail.com', '$1$K/3.LX/.$YVYo9M3fwa.tb2HM3e6xz0', 'equipier', 0),
(8, 'momo', 'momo', 'moua@moua.fr', '$1$2L2.hm/.$ZNUPATUmx37AxRmucGYBn/', 'chefequipe', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
