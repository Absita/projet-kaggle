-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 16 Décembre 2015 à 04:12
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bd_kaggle`
--

-- --------------------------------------------------------

--
-- Structure de la table `réunion_reu`
--

CREATE TABLE IF NOT EXISTS `réunion_reu` (
  `Id_reu` int(200) NOT NULL AUTO_INCREMENT,
  `Titre_reu` text NOT NULL,
  `Date_reu` date NOT NULL,
  `Type_reu` varchar(200) NOT NULL,
  `Lieu_reu` varchar(200) NOT NULL,
  `Heure_reu` varchar(200) NOT NULL,
  `Id_equ_reu` int(200) NOT NULL,
  PRIMARY KEY (`Id_reu`),
  KEY `Id_equ_reu` (`Id_equ_reu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `réunion_reu`
--

INSERT INTO `réunion_reu` (`Id_reu`, `Titre_reu`, `Date_reu`, `Type_reu`, `Lieu_reu`, `Heure_reu`, `Id_equ_reu`) VALUES
(1, 'match', '2015-12-02', 'Reunion ordinaire', 'paris', '1h00', 0),
(2, 'match', '2015-12-02', 'Reunion ordinaire', 'paris', '1h00', 0),
(3, 'allo', '2015-12-17', 'Reunion ordinaire', 'rome', '17h00', 0),
(4, 'preparatifs', '2015-12-27', 'Reunion urgente', 'Saint Louis', '9h00', 0),
(5, 'retour', '2015-12-21', 'Reunion ordinaire', 'esigelec', '10h00', 0),
(6, 'triomphe', '2015-12-17', 'Visio conference', 'thies', '8h00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
