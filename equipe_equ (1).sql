-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 16 Décembre 2015 à 04:13
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bd_kaggle`
--

-- --------------------------------------------------------

--
-- Structure de la table `equipe_equ`
--

CREATE TABLE IF NOT EXISTS `equipe_equ` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(200) NOT NULL,
  `Description` text NOT NULL,
  `Id_cep` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `Id_cep` (`Id_cep`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `equipe_equ`
--

INSERT INTO `equipe_equ` (`Id`, `Nom`, `Description`, `Id_cep`) VALUES
(1, 'Nom', '', 0),
(2, 'Nom', 'Description', 0),
(3, 'Nom', 'Description', 0),
(4, 'woooo', '', 0),
(5, 'woooo', '', 0),
(6, 'absita', '', 0),
(7, 'w', '', 0),
(8, 'w', '', 0),
(9, 'absita', '', 0),
(10, 'absita', '', 0),
(11, 'absita', '', 0),
(12, 'absita', 'tcha kaw', 0),
(13, 'allo', 'b1 ou ki', 0),
(14, 'allo', 'b1 ou ki', 0),
(15, 'allo', 'b1 ou ki', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
