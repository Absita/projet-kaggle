<!DOCTYPE html>
<html>
<head

 <link rel="stylesheet" href="style_pro.css">
  <meta charset="utf-8">
  <title>Index</title>
  
</head>
<body>
<?php
require_once('param.inc.php');
?>
<div> <?php include('banniere.inc.php'); ?></div>

<img class="floating-box0" src="https://jessireel.files.wordpress.com/2009/11/kaggle-monster2.png" alt="Mountain View"/>
<div class="flex-itemA" >
<p>Kaggle is the world's largest community of data scientists. </p>
<p>They compete with each other to solve complex data science problems,
and the top competitors are invited to work on the most interesting and sensitive business problems from some of the world’s biggest companies through Masters competitions.</p>
<p></p>
<p>Kaggle provides cutting-edge data science results to companies of all sizes.</p>
<p>We have a proven track-record of solving real-world problems across a diverse array of industries including life sciences,
financial services, energy, information technology, and retail.</p>
</div>

<div> <?php include('pied.inc.php'); ?></div>

</body>
</html>
